package android.humanbooster.sqlitedemo;

import android.content.Context;

import android.content.CursorLoader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.humanbooster.sqlitedemo.dao.MovieDAO;



public class MyCursorLoader extends CursorLoader {

    private SQLiteDatabase db;

    public MyCursorLoader(Context context, SQLiteDatabase db) {
        super(context);
        this.db = db;
    }


    @Override
    protected Cursor onLoadInBackground() {
        //async
        return MovieDAO.getAll(db);
    }
}
