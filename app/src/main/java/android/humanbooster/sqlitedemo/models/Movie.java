package android.humanbooster.sqlitedemo.models;



public class Movie {

    private Long id;
    private String title;

    public Movie() {
    }

    public Movie(String movie) {
        title = movie;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}



